﻿namespace AnalyzeConcentration.WinForms
{
    partial class ImportFileAnalyze
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ImportFileAnalyze));
            this.BtnCarregar = new System.Windows.Forms.Button();
            this.btnStartAnalyse = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.txbFilePath = new System.Windows.Forms.TextBox();
            this.lblFilePath = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.clmAmostra = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmAbs = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ClnConcentracao = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ClnInterpretacao = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txbAbsorbanceMax1 = new System.Windows.Forms.TextBox();
            this.txbConcentrationMax = new System.Windows.Forms.TextBox();
            this.txbAbsorbanceMax2 = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txbAbsorbanceMin2 = new System.Windows.Forms.TextBox();
            this.txbAbsorbanceMin1 = new System.Windows.Forms.TextBox();
            this.txbConcentrationMin = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnExport = new System.Windows.Forms.Button();
            this.btnClearData = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.btnColar = new System.Windows.Forms.Button();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // BtnCarregar
            // 
            this.BtnCarregar.Image = ((System.Drawing.Image)(resources.GetObject("BtnCarregar.Image")));
            this.BtnCarregar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BtnCarregar.Location = new System.Drawing.Point(683, 33);
            this.BtnCarregar.Margin = new System.Windows.Forms.Padding(4);
            this.BtnCarregar.Name = "BtnCarregar";
            this.BtnCarregar.Size = new System.Drawing.Size(300, 47);
            this.BtnCarregar.TabIndex = 0;
            this.BtnCarregar.Text = "CARREGAR ARQUIVO PARA ANÁLISE";
            this.BtnCarregar.UseVisualStyleBackColor = true;
            this.BtnCarregar.Click += new System.EventHandler(this.BtnCarregar_Click);
            // 
            // btnStartAnalyse
            // 
            this.btnStartAnalyse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStartAnalyse.Image = ((System.Drawing.Image)(resources.GetObject("btnStartAnalyse.Image")));
            this.btnStartAnalyse.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnStartAnalyse.Location = new System.Drawing.Point(683, 161);
            this.btnStartAnalyse.Margin = new System.Windows.Forms.Padding(4);
            this.btnStartAnalyse.Name = "btnStartAnalyse";
            this.btnStartAnalyse.Size = new System.Drawing.Size(265, 47);
            this.btnStartAnalyse.TabIndex = 1;
            this.btnStartAnalyse.Text = "INICIAR ANÁLISE DE CONCENTRAÇÃO";
            this.btnStartAnalyse.UseVisualStyleBackColor = true;
            this.btnStartAnalyse.Click += new System.EventHandler(this.btnStartAnalyse_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.Title = "Localizar Arquivo para Analise";
            // 
            // txbFilePath
            // 
            this.txbFilePath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txbFilePath.Location = new System.Drawing.Point(683, 107);
            this.txbFilePath.Margin = new System.Windows.Forms.Padding(4);
            this.txbFilePath.Name = "txbFilePath";
            this.txbFilePath.ReadOnly = true;
            this.txbFilePath.Size = new System.Drawing.Size(263, 23);
            this.txbFilePath.TabIndex = 2;
            // 
            // lblFilePath
            // 
            this.lblFilePath.AutoSize = true;
            this.lblFilePath.Location = new System.Drawing.Point(683, 83);
            this.lblFilePath.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblFilePath.Name = "lblFilePath";
            this.lblFilePath.Size = new System.Drawing.Size(143, 17);
            this.lblFilePath.TabIndex = 3;
            this.lblFilePath.Text = "Arquivo para Analise:";
            // 
            // progressBar1
            // 
            this.progressBar1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBar1.Location = new System.Drawing.Point(13, 348);
            this.progressBar1.Margin = new System.Windows.Forms.Padding(4);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(935, 28);
            this.progressBar1.Step = 0;
            this.progressBar1.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.progressBar1.TabIndex = 5;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowDrop = true;
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.clmAmostra,
            this.clmAbs,
            this.ClnConcentracao,
            this.ClnInterpretacao});
            this.dataGridView1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.dataGridView1.Location = new System.Drawing.Point(13, 205);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(4);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(646, 135);
            this.dataGridView1.TabIndex = 7;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            this.dataGridView1.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellValueChanged);
            this.dataGridView1.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dataGridView1_EditingControlShowing);
            this.dataGridView1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dataGridView1_KeyPress);
            // 
            // clmAmostra
            // 
            this.clmAmostra.Frozen = true;
            this.clmAmostra.HeaderText = "Amostra";
            this.clmAmostra.Name = "clmAmostra";
            this.clmAmostra.Width = 150;
            // 
            // clmAbs
            // 
            this.clmAbs.Frozen = true;
            this.clmAbs.HeaderText = "Abs_450nm";
            this.clmAbs.Name = "clmAbs";
            this.clmAbs.Width = 150;
            // 
            // ClnConcentracao
            // 
            this.ClnConcentracao.HeaderText = "[Vitamina D] (ng/dL)";
            this.ClnConcentracao.Name = "ClnConcentracao";
            this.ClnConcentracao.Width = 150;
            // 
            // ClnInterpretacao
            // 
            this.ClnInterpretacao.HeaderText = "Observação";
            this.ClnInterpretacao.Name = "ClnInterpretacao";
            this.ClnInterpretacao.Width = 300;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(13, 17);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(646, 138);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Calibração";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.txbAbsorbanceMax1);
            this.groupBox3.Controls.Add(this.txbConcentrationMax);
            this.groupBox3.Controls.Add(this.txbAbsorbanceMax2);
            this.groupBox3.Location = new System.Drawing.Point(349, 24);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(226, 100);
            this.groupBox3.TabIndex = 25;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Calibrador 2";
            // 
            // txbAbsorbanceMax1
            // 
            this.txbAbsorbanceMax1.Location = new System.Drawing.Point(12, 21);
            this.txbAbsorbanceMax1.Margin = new System.Windows.Forms.Padding(4);
            this.txbAbsorbanceMax1.Name = "txbAbsorbanceMax1";
            this.txbAbsorbanceMax1.Size = new System.Drawing.Size(93, 23);
            this.txbAbsorbanceMax1.TabIndex = 3;
            this.txbAbsorbanceMax1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txbAbsorbanceMax1_KeyPress);
            // 
            // txbConcentrationMax
            // 
            this.txbConcentrationMax.Location = new System.Drawing.Point(12, 59);
            this.txbConcentrationMax.Margin = new System.Windows.Forms.Padding(4);
            this.txbConcentrationMax.Name = "txbConcentrationMax";
            this.txbConcentrationMax.Size = new System.Drawing.Size(93, 23);
            this.txbConcentrationMax.TabIndex = 6;
            this.txbConcentrationMax.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txbConcentrationMax_KeyPress);
            // 
            // txbAbsorbanceMax2
            // 
            this.txbAbsorbanceMax2.Location = new System.Drawing.Point(123, 21);
            this.txbAbsorbanceMax2.Margin = new System.Windows.Forms.Padding(4);
            this.txbAbsorbanceMax2.Name = "txbAbsorbanceMax2";
            this.txbAbsorbanceMax2.Size = new System.Drawing.Size(93, 23);
            this.txbAbsorbanceMax2.TabIndex = 4;
            this.txbAbsorbanceMax2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txbAbsorbanceMax2_KeyPress);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txbAbsorbanceMin2);
            this.groupBox2.Controls.Add(this.txbAbsorbanceMin1);
            this.groupBox2.Controls.Add(this.txbConcentrationMin);
            this.groupBox2.Location = new System.Drawing.Point(110, 24);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(226, 100);
            this.groupBox2.TabIndex = 24;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Calibrador 1";
            // 
            // txbAbsorbanceMin2
            // 
            this.txbAbsorbanceMin2.Location = new System.Drawing.Point(122, 21);
            this.txbAbsorbanceMin2.Margin = new System.Windows.Forms.Padding(4);
            this.txbAbsorbanceMin2.Name = "txbAbsorbanceMin2";
            this.txbAbsorbanceMin2.Size = new System.Drawing.Size(93, 23);
            this.txbAbsorbanceMin2.TabIndex = 2;
            this.txbAbsorbanceMin2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txbAbsorbanceMin2_KeyPress);
            // 
            // txbAbsorbanceMin1
            // 
            this.txbAbsorbanceMin1.Location = new System.Drawing.Point(10, 21);
            this.txbAbsorbanceMin1.Margin = new System.Windows.Forms.Padding(4);
            this.txbAbsorbanceMin1.Name = "txbAbsorbanceMin1";
            this.txbAbsorbanceMin1.Size = new System.Drawing.Size(93, 23);
            this.txbAbsorbanceMin1.TabIndex = 1;
            this.txbAbsorbanceMin1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txbAbsorbanceMin1_KeyPress);
            // 
            // txbConcentrationMin
            // 
            this.txbConcentrationMin.Location = new System.Drawing.Point(10, 59);
            this.txbConcentrationMin.Margin = new System.Windows.Forms.Padding(4);
            this.txbConcentrationMin.Name = "txbConcentrationMin";
            this.txbConcentrationMin.Size = new System.Drawing.Size(93, 23);
            this.txbConcentrationMin.TabIndex = 5;
            this.txbConcentrationMin.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txbConcentrationMin_KeyPress);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(25, 91);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(54, 17);
            this.label5.TabIndex = 16;
            this.label5.Text = "(ng/dL)";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 35);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(98, 17);
            this.label1.TabIndex = 12;
            this.label1.Text = "Absorbâncias:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(2, 73);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(100, 17);
            this.label4.TabIndex = 16;
            this.label4.Text = "Concentração:";
            // 
            // btnExport
            // 
            this.btnExport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExport.Enabled = false;
            this.btnExport.Image = ((System.Drawing.Image)(resources.GetObject("btnExport.Image")));
            this.btnExport.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExport.Location = new System.Drawing.Point(683, 216);
            this.btnExport.Margin = new System.Windows.Forms.Padding(4);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(265, 47);
            this.btnExport.TabIndex = 14;
            this.btnExport.Text = "EXPORTAR DADOS PARA EXCEL";
            this.btnExport.UseVisualStyleBackColor = true;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // btnClearData
            // 
            this.btnClearData.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClearData.Image = ((System.Drawing.Image)(resources.GetObject("btnClearData.Image")));
            this.btnClearData.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnClearData.Location = new System.Drawing.Point(683, 294);
            this.btnClearData.Margin = new System.Windows.Forms.Padding(4);
            this.btnClearData.Name = "btnClearData";
            this.btnClearData.Size = new System.Drawing.Size(265, 47);
            this.btnClearData.TabIndex = 15;
            this.btnClearData.Text = "LIMPAR DADOS";
            this.btnClearData.UseVisualStyleBackColor = true;
            this.btnClearData.Click += new System.EventHandler(this.btnClearData_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 167);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(211, 17);
            this.label2.TabIndex = 16;
            this.label2.Text = "Para colar os dados clique aqui:";
            // 
            // btnColar
            // 
            this.btnColar.Location = new System.Drawing.Point(223, 164);
            this.btnColar.Name = "btnColar";
            this.btnColar.Size = new System.Drawing.Size(150, 28);
            this.btnColar.TabIndex = 17;
            this.btnColar.Text = "COLAR - (CTRL-V)";
            this.btnColar.UseVisualStyleBackColor = true;
            this.btnColar.Click += new System.EventHandler(this.btnColar_Click);
            // 
            // linkLabel1
            // 
            this.linkLabel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Location = new System.Drawing.Point(853, 8);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(95, 17);
            this.linkLabel1.TabIndex = 18;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "Abrir ReadMe";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(726, 8);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(123, 17);
            this.label3.TabIndex = 26;
            this.label3.Text = "Precisa de Ajuda?";
            // 
            // ImportFileAnalyze
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(974, 391);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.linkLabel1);
            this.Controls.Add(this.btnColar);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnClearData);
            this.Controls.Add(this.btnExport);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.lblFilePath);
            this.Controls.Add(this.txbFilePath);
            this.Controls.Add(this.btnStartAnalyse);
            this.Controls.Add(this.BtnCarregar);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "ImportFileAnalyze";
            this.Text = "bindingcalc - VD - Versão 1.3";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.ImportFileAnalyze_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BtnCarregar;
        private System.Windows.Forms.Button btnStartAnalyse;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.TextBox txbFilePath;
        private System.Windows.Forms.Label lblFilePath;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnExport;
        private System.Windows.Forms.Button btnClearData;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox txbAbsorbanceMax1;
        private System.Windows.Forms.TextBox txbConcentrationMax;
        private System.Windows.Forms.TextBox txbAbsorbanceMax2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txbAbsorbanceMin2;
        private System.Windows.Forms.TextBox txbAbsorbanceMin1;
        private System.Windows.Forms.TextBox txbConcentrationMin;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnColar;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmAmostra;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmAbs;
        private System.Windows.Forms.DataGridViewTextBoxColumn ClnConcentracao;
        private System.Windows.Forms.DataGridViewTextBoxColumn ClnInterpretacao;
    }
}