﻿using AnaliseConcentration.Entities;
using AnalyzeConcentration.Business;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AnalyzeConcentration.WinForms
{
    public partial class ImportFileAnalyze : Form
    {
        private bool done = false;
        public AnalyseData AnalyseData { get; set; }
        public ImportFileAnalyze()
        {
            InitializeComponent();
            AnalyseData = new AnalyseData();
        }

        private void BtnCarregar_Click(object sender, EventArgs e)
        {
            ClearData();
            txbFilePath.Text = "";
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            DialogResult dr = openFileDialog1.ShowDialog();

            if (dr == DialogResult.OK)
            {
                progressBar1.Value = 0;
                foreach (String arquivo in openFileDialog1.FileNames)
                {
                    txbFilePath.Text += arquivo;
                    btnStartAnalyse.Enabled = true;
                }

                if (!string.IsNullOrEmpty(txbFilePath.Text))
                {

                    var file = new FileUpload();
                    AnalyseData = file.ReadFile(txbFilePath.Text);

                    txbAbsorbanceMax1.Text = AnalyseData.CalibrationCurve.AbsorbanceValues.ValueReferenceMax.ToString();
                    txbAbsorbanceMin1.Text = AnalyseData.CalibrationCurve.AbsorbanceValues.ValueReferenceZero.ToString();

                    txbConcentrationMax.Text = AnalyseData.CalibrationCurve.ConcentrationValues.ValueReferenceMax.ToString();
                    txbConcentrationMin.Text = AnalyseData.CalibrationCurve.ConcentrationValues.ValueReferenceZero.ToString();

                    LoadGrid(AnalyseData);
                }
            }
        }

        private void btnStartAnalyse_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(txbAbsorbanceMin1.Text) &&
                    !string.IsNullOrEmpty(txbAbsorbanceMax1.Text) &&
                    !string.IsNullOrEmpty(txbConcentrationMin.Text) &&
                    !string.IsNullOrEmpty(txbConcentrationMax.Text))
                {
                    if (!(dataGridView1.Rows.Count > 1))
                    {
                        MessageBox.Show("Por favor, insira pelo menos uma amostra antes de iniciar a análise.", "Atenção");
                        return;
                    }

                    ReadGrid();

                    progressBar1.Value = 10;

                    var absorbanceMin = Convert.ToDouble(txbAbsorbanceMin1.Text);
                    if (!string.IsNullOrEmpty(txbAbsorbanceMin2.Text))
                        absorbanceMin = ((Convert.ToDouble(txbAbsorbanceMin1.Text) + Convert.ToDouble(txbAbsorbanceMin2.Text)) / 2);

                    var absorbanceMax = Convert.ToDouble(txbAbsorbanceMax1.Text);
                    if (!string.IsNullOrEmpty(txbAbsorbanceMax2.Text))
                        absorbanceMax = ((Convert.ToDouble(txbAbsorbanceMax1.Text) + Convert.ToDouble(txbAbsorbanceMax2.Text)) / 2);

                    AnalyseData.CalibrationCurve.AbsorbanceValues.ValueReferenceMax = absorbanceMax;
                    AnalyseData.CalibrationCurve.AbsorbanceValues.ValueReferenceZero = absorbanceMin;

                    AnalyseData.CalibrationCurve.ConcentrationValues.ValueReferenceMax = Convert.ToDouble(txbConcentrationMax.Text);
                    AnalyseData.CalibrationCurve.ConcentrationValues.ValueReferenceZero = Convert.ToDouble(txbConcentrationMin.Text);

                    ReadGrid();

                    if (AnalyseData != null && AnalyseData.Samples.Count > 0)
                    {
                        progressBar1.Value = 20;


                        var compute = new ComputeConcentration();
                        var exames = compute.AnalyzeConcentration(AnalyseData);
                        AnalyseData.Samples = exames;

                        LoadGrid(AnalyseData);
                        progressBar1.Increment(100);
                        btnExport.Enabled = true;

                        MessageBox.Show("Dados Analisados com Sucesso", "Analise",
                                        MessageBoxButtons.OK,
                                        MessageBoxIcon.Information);

                    }
                    else
                    {
                        MessageBox.Show("Por favor, selecione o arquivo a ser analisado.");
                    }
                }
                else
                {
                    MessageBox.Show("Por favor, informe as absorbâncias e concentrações dos calibradores.", "Atenção");
                }
            }
            catch (SecurityException ex)
            {
                MessageBox.Show("Erro de segurança Contate o administrador de segurança da rede.\n\n" +
                                            "Mensagem : " + ex.Message + "\n\n" +
                                            "Detalhes (enviar ao suporte):\n\n" + ex.StackTrace);
            }
        }

        private void ImportFileAnalyze_Load(object sender, EventArgs e)
        {

        }

        private void dataGridView1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Convert.ToInt32(e.KeyChar) == 22)
                PasteClipboard(0);
        }

        private void PasteClipboard(int coluna)
        {
            var data = new AnalyseData();
            var dictionary = new Dictionary<string, string>();
            var clipboard = Clipboard.GetText();

            string[] lines = clipboard.Split(new[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);

            if (lines[0].Split('\t').Count() == 2)
            {
                foreach (var lineConcat in lines)
                {
                    var line  = lineConcat.Split('\t');
                    var id = line[0];
                    var absorbance = line[1];

                    double value = 0;
                    if (double.TryParse(absorbance, out value))
                        data.Samples.Add(new Sample() { Description = id, Absorbanse = Convert.ToDouble(absorbance.Replace("\r", "")) });
                    else
                    {
                        MessageBox.Show("A coluna de absorbância deve conter somente valores numéricos.", "Atenção");
                        break;
                    }
                }


                //dictionary = lines.ToDictionary(s => s.Split('\t')[0], s => s.Split('\t')[1]);
                //foreach (var item in dictionary)
                //{
                //    double value = 0;
                //    if (double.TryParse(item.Value, out value))
                //        data.Samples.Add(new Sample() { Description = item.Key, Absorbanse = Convert.ToDouble(item.Value.Replace("\r", "")) });
                //    else
                //    {
                //        MessageBox.Show("A coluna de absorbância deve conter somente valores numéricos.", "Atenção");
                //        break;
                //    }
                //}
            }
            else if (lines[0].Split('\t').Count() == 1)
            {
                var dictionary2 = lines.ToDictionary(s => s.Split('\t')[0]);
                foreach (var item in dictionary2)
                {
                    var sample = new Sample();
                    if (dataGridView1.CurrentCell.ColumnIndex == 0)
                    {
                        sample.Description = item.Key;
                        data.Samples.Add(sample);
                    }
                    else if (dataGridView1.CurrentCell.ColumnIndex == 1)
                    {
                        double value = 0;
                        double.TryParse(item.Value.Replace("\r", ""), out value);
                        if (value > 0)
                        {
                            sample.Absorbanse = value;
                            data.Samples.Add(sample);
                        }
                    }

                }
            }

            LoadGrid(data);
            AnalyseData = data;
            btnStartAnalyse.Enabled = true;
        }

        private void LoadGrid(AnalyseData data)
        {
            var row = 0;
            if (dataGridView1.Rows.Count < data.Samples.Count)
                dataGridView1.Rows.Add(data.Samples.Count);

            foreach (var item in data.Samples)
            {
                dataGridView1[0, row].Value = item.Description;

                if (item.Absorbanse > 0)
                    dataGridView1[1, row].Value = item.Absorbanse;

                if (item.Concentration > 0)
                {
                    dataGridView1[2, row].Value = item.Concentration;
                    // dataGridView1[3, row].Value = item.Interpretation;
                }
                else if (!string.IsNullOrEmpty(item.Interpretation))
                {
                    dataGridView1[2, row].Value = "*";
                    dataGridView1[3, row].Value = item.Interpretation;
                }
                else
                {
                    dataGridView1[2, row].Value = "";
                    dataGridView1[3, row].Value = "";
                }

                row++;
            }
        }

        private void ReadGrid()
        {
            var data = new List<Sample>();
            var dadosValidos = true;

            for (int row = 0; row < dataGridView1.Rows.Count - 1; row++)
            {
                var sample = new Sample();
                if (dataGridView1.Rows.Count >= (row + 1))
                {
                    double value = 0;
                    if (double.TryParse(dataGridView1[1, row].Value.ToString(), out value))
                    {
                        sample.Description = Convert.ToString(dataGridView1[0, row].Value);
                        sample.Absorbanse = Convert.ToDouble(dataGridView1[1, row].Value);
                        data.Add(sample);
                    }
                    else
                    {
                        dadosValidos = false;
                        MessageBox.Show("A coluna de absorbância (Abs_450nm) deve conter somente valores numéricos.", "Atenção");
                        break;
                    }
                }
            }

            if (dadosValidos)
                AnalyseData.Samples = data;
        }

        private void LoadFile()
        {
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            var file = new FileUpload();
            file.WriteFile(AnalyseData.Samples);
            System.Diagnostics.Process.Start("C:\\AnaliseAbsorbanciaConcentracao\\");
        }

        private void btnClearData_Click(object sender, EventArgs e)
        {
            ClearData();
        }

        private void ClearData()
        {
            dataGridView1.Rows.Clear();
            txbAbsorbanceMax1.Text = string.Empty;
            txbAbsorbanceMin1.Text = string.Empty;
            txbAbsorbanceMax2.Text = string.Empty;
            txbAbsorbanceMin2.Text = string.Empty;
            txbConcentrationMax.Text = string.Empty;
            txbConcentrationMin.Text = string.Empty;
            txbFilePath.Text = string.Empty;
            btnExport.Enabled = false;
        }

        private void btnColar_Click(object sender, EventArgs e)
        {
            PasteClipboard(0);
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            new frmReadMe().Show();
        }

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("explorer.exe", @"C:\Program Files\Ampligenix");
        }

        private void dataGridView1_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            e.Control.KeyPress -= new KeyPressEventHandler(clmAbs_KeyPress);
            if (dataGridView1.CurrentCell.ColumnIndex == 1) //Desired Column
            {
                TextBox tb = e.Control as TextBox;
                if (tb != null)
                {
                    tb.KeyPress += new KeyPressEventHandler(clmAbs_KeyPress);
                }
            }
        }

        private void clmAbs_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != ','))
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if ((e.KeyChar == ',') && ((sender as TextBox).Text.IndexOf(',') > -1))
            {
                e.Handled = true;
            }
        }

        private void dataGridView1_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {


        }

        private void txbAbsorbanceMin1_KeyPress(object sender, KeyPressEventArgs e)
        {
            SomenteNumeros(sender, e);
        }

        private void SomenteNumeros(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != ','))
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if ((e.KeyChar == ',') && ((sender as TextBox).Text.IndexOf(',') > -1))
            {
                e.Handled = true;
            }
        }

        private void txbAbsorbanceMin2_KeyPress(object sender, KeyPressEventArgs e)
        {
            SomenteNumeros(sender, e);
        }

        private void txbConcentrationMin_KeyPress(object sender, KeyPressEventArgs e)
        {
            SomenteNumeros(sender, e);
        }

        private void txbAbsorbanceMax1_KeyPress(object sender, KeyPressEventArgs e)
        {
            SomenteNumeros(sender, e);
        }

        private void txbAbsorbanceMax2_KeyPress(object sender, KeyPressEventArgs e)
        {
            SomenteNumeros(sender, e);
        }

        private void txbConcentrationMax_KeyPress(object sender, KeyPressEventArgs e)
        {
            SomenteNumeros(sender, e);
        }
    }
}
