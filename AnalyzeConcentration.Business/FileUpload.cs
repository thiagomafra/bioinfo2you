﻿using AnaliseConcentration.Entities;
using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace AnalyzeConcentration.Business
{
    public class FileUpload
    {
        public AnalyseData ReadFile(string filePath)
        {
            var AnalyseData = new AnalyseData();
            if (!string.IsNullOrEmpty(filePath))
            {
                var excelApp = new Microsoft.Office.Interop.Excel.Application();
                var workbook = excelApp.Workbooks.Open(filePath, 0, true, 5, "", "", true, XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
                var worksheet = (Worksheet)workbook.Worksheets.get_Item(1);
                var range = worksheet.UsedRange;

                var calibrationCurve = GetCalibrationCurve(range);
                var samples = GetSamples(range);                
                AnalyseData.CalibrationCurve = calibrationCurve;
                AnalyseData.Samples = samples;

                workbook.Close();
                excelApp.Quit();

                try
                {
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(worksheet);
                    worksheet = null;
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(workbook);
                    workbook = null;
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(excelApp);
                    excelApp = null;
                }
                catch (Exception ex)
                {
                    worksheet = null;
                }
                finally
                {
                    GC.Collect();
                }
            }
            return AnalyseData;
        }

        public void WriteFile(List<Sample> exams)
        {
            Workbook xlWorkBook;
            Worksheet xlWorkSheet;
            object misValue = System.Reflection.Missing.Value;
            var xlApp = new Microsoft.Office.Interop.Excel.Application();


            xlWorkBook = xlApp.Workbooks.Add(misValue);
            xlWorkSheet = (Worksheet)xlWorkBook.Worksheets.get_Item(1);

            xlWorkSheet.Cells[1, 1] = "Referencia";
            xlWorkSheet.Cells[1, 2] = "Abs_450nm";
            xlWorkSheet.Cells[1, 3] = "Concentração";
            xlWorkSheet.Cells[1, 4] = "Interpretação";

            int rowCont = 2;
            foreach (var item in exams)
            {
                xlWorkSheet.Cells[rowCont, 1] = item.Description;
                xlWorkSheet.Cells[rowCont, 2] = item.Absorbanse;
                xlWorkSheet.Cells[rowCont, 3] = item.Concentration;
                xlWorkSheet.Cells[rowCont, 4] = item.Interpretation;
                rowCont++;
            }

            string pathFolder = "C:\\AnaliseAbsorbanciaConcentracao";

            if (!Directory.Exists(pathFolder))
                Directory.CreateDirectory(pathFolder);

            string pathFileOut = string.Format(pathFolder + "{0}{1}{2}", "\\AnaliseFinal_", DateTime.Now.ToString("ddmmyyyyhhmmss"), ".xls");

            xlWorkBook.SaveAs(pathFileOut, XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue, XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);
            xlWorkBook.Close(true, misValue, misValue);
            xlApp.Quit();

            Marshal.ReleaseComObject(xlWorkSheet);
            Marshal.ReleaseComObject(xlWorkBook);
            Marshal.ReleaseComObject(xlApp);
        }

        private CalibrationCurve GetCalibrationCurve(Range range)
        {
            var values = new CalibrationCurve();

            var columnAbsorbance = 2;
            var columnConcentration = 3;

            var rowAbsorbanceAndConcentrationZero = 2;
            var rowAbsorbanceAndConcentrationMax = 3;

            var absorbanceZero = (range.Cells[rowAbsorbanceAndConcentrationZero, columnAbsorbance] as Range).Value2.ToString();
            var concentrationZero = (range.Cells[rowAbsorbanceAndConcentrationZero, columnConcentration] as Range).Value2.ToString();

            var absorbanceMax = (range.Cells[rowAbsorbanceAndConcentrationMax, columnAbsorbance] as Range).Value2.ToString();
            var concentrationMax = (range.Cells[rowAbsorbanceAndConcentrationMax, columnConcentration] as Range).Value2.ToString();

            values.AbsorbanceValues.ValueReferenceZero = Convert.ToDouble(absorbanceZero);
            values.AbsorbanceValues.ValueReferenceMax = Convert.ToDouble(absorbanceMax);
            values.ConcentrationValues.ValueReferenceZero = Convert.ToDouble(concentrationZero);
            values.ConcentrationValues.ValueReferenceMax = Convert.ToDouble(concentrationMax);

            return values;
        }

        private List<Sample> GetSamples(Range range)
        {
            var result = new List<Sample>();

            // Parametrizar estes dados
            var sampleStartRow = 5;

            for (var row = sampleStartRow; row < range.Rows.Count + 1; row++)
            {
                if ((range.Cells[row, 2] as Range).Value2 != null)
                {

                    double value = 0;
                    if (!double.TryParse(Convert.ToString((range.Cells[row, 2] as Range).Value2), out value))
                    {
                        MessageBox.Show("A coluna de absorbância (Abs_450nm) deve conter somente valores numéricos.", "Atenção");
                        break;
                    }

                    var sample = new Sample();
                    sample.Description = (range.Cells[row, 1] as Range).Value2.ToString();
                    sample.Absorbanse = Convert.ToDouble((range.Cells[row, 2] as Range).Value2);
                    result.Add(sample);
                }
            }

            return result;
        }
    }
}
