﻿using AnaliseConcentration.Entities;
using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;

namespace AnalyzeConcentration.Business
{
    public class ComputeConcentration
    {
        public List<Sample> AnalyzeConcentration(AnalyseData analyseData)
        {
            var angularCoefficient = ComputeAngularCoefficient(analyseData.CalibrationCurve);
            var linearCoefficient = ComputeOccupationPercentage(analyseData.CalibrationCurve.AbsorbanceValues.ValueReferenceZero,
                                                                analyseData.CalibrationCurve.AbsorbanceValues.ValueReferenceZero);

            var exams = new List<Sample>();
            exams.AddRange(analyseData.Samples);

            foreach (var sample in analyseData.Samples)
            {
                if (analyseData.CalibrationCurve.AbsorbanceValues.ValueReferenceMax > sample.Absorbanse)
                {
                    exams[analyseData.Samples.IndexOf(sample)].Interpretation = "Fora da curva – diluir amostra";
                }                
                else
                {
                    var concentrationValue = ComputeConcetration(sample.Absorbanse, analyseData.CalibrationCurve, angularCoefficient, linearCoefficient);
                    var amostraDuplicada = exams.Where(x => x.Absorbanse == sample.Absorbanse).Count() > 1;
                    var primeiraAmostraDuplicadaPreenchida = exams.Where(x => x.Absorbanse == sample.Absorbanse && x.Concentration > 0).Count() > 0;

                    //if (amostraDuplicada && primeiraAmostraDuplicadaPreenchida)
                    //    continue;

                    exams.Where(x => x.Description == sample.Description && x.Absorbanse == sample.Absorbanse).FirstOrDefault().Concentration = Math.Round(concentrationValue, 1);

                }

                //if (sample.Absorbanse > 0 && sample.Concentration >= 0)
                //{
                //    exams[analyseData.Samples.IndexOf(sample)].Interpretation = "Valores abaixo do limite de detecção";
                //}
            }

            return exams;
        }

        private double ComputeOccupationPercentage(double absorbance1, double absorbance2)
        {
            var value = Math.Log((absorbance1 / absorbance2) * 100, 10);
            return value;
        }

        private double ComputeAngularCoefficient(CalibrationCurve calibrationCurve)
        {
            var absorbanceZero = calibrationCurve.AbsorbanceValues.ValueReferenceZero;
            var concentrationZero = calibrationCurve.ConcentrationValues.ValueReferenceZero;

            var absorbanceMax = calibrationCurve.AbsorbanceValues.ValueReferenceMax;
            var concentrationMax = calibrationCurve.ConcentrationValues.ValueReferenceMax;

            var logOccupation1 = ComputeOccupationPercentage(absorbanceMax, absorbanceMax);
            var logOccupation2 = ComputeOccupationPercentage(absorbanceZero, absorbanceMax);

            var angularCoefficient = (logOccupation2 - logOccupation1) / (concentrationMax - concentrationZero);

            return angularCoefficient;
        }

        private double ComputeConcetration(double sampleAbsorbance, CalibrationCurve calibrationCurve, double angularCoefficient, double blinearCoefficient)
        {
            var refAbsorbanceZero = calibrationCurve.AbsorbanceValues.ValueReferenceZero;
            var tmp1 = Math.Log((sampleAbsorbance / refAbsorbanceZero) * 100, 10);
            var tmp2 = (tmp1 - blinearCoefficient) / angularCoefficient;
            return Math.Abs(tmp2);
        }
    }
}

