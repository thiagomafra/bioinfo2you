﻿namespace AnaliseConcentration.Entities
{
    public class Sample
    {
        public string Description { get; set; }
        public double Absorbanse { get; set; }
        public double Concentration { get; set; }
        public string Interpretation { get; set; }
    }
}
