﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AnaliseConcentration.Entities
{
    public class Absorbance
    {
        public double ValueReferenceZero { get; set; }
        public double ValueReferenceMax { get; set; }
    }
}
