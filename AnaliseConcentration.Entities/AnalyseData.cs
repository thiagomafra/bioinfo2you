﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AnaliseConcentration.Entities
{
    public class AnalyseData
    {
        public CalibrationCurve CalibrationCurve { get; set; }
        public List<Sample> Samples { get; set; }

        public AnalyseData()
        {
            CalibrationCurve = new CalibrationCurve();
            Samples = new List<Sample>();
        }
    }
}
