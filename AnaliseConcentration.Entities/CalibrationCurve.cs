﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AnaliseConcentration.Entities
{
    public class CalibrationCurve
    {
        public Absorbance AbsorbanceValues { get; set; }
        public Concentration ConcentrationValues { get; set; }

        public CalibrationCurve()
        {
            AbsorbanceValues = new Absorbance();
            ConcentrationValues = new Concentration();
        }
    }
}
